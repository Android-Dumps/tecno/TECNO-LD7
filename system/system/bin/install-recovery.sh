#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:ca607d442a197112b4fa201a6addab701b4acc61; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:cbec6f0f29c8f91713af6dfd867720d63fd30285 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:ca607d442a197112b4fa201a6addab701b4acc61 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
